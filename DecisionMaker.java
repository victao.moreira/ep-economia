package com.ep.economia;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DecisionMaker {
    private JButton calcularButton;
    private JTextField precoInicial;
    private JTextField precoFinal;
    private JTextField quantidadeInicial;
    private JTextField quantidadeFinal;
    private JPanel decisionView;
    private JTextArea elasticidadePD;


    public DecisionMaker() {
        calcularButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elasticidadePD.setText(calculate());
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("DecisionMaker");
        frame.setContentPane(new DecisionMaker().decisionView);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private String calculate(){
        String resposta;
        try {
            float precoFim = Integer.parseInt(precoFinal.getText());
            float precoIni = Integer.parseInt(precoInicial.getText());
            float quantidadeFim = Integer.parseInt(quantidadeFinal.getText());
            float quantidadeIni = Integer.parseInt(quantidadeInicial.getText());
            resposta = "" + ((quantidadeFim - quantidadeIni) / quantidadeIni) / ((precoFim - precoIni) / precoIni);
        } catch(Exception e){
            resposta = "insira valores validos";
        }
        return resposta;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}


