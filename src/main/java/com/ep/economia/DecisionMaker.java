package com.ep.economia;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DecisionMaker {
    private JButton calcularButton;
    private JTextField parametro1;
    private JTextField parametro2;
    private JTextField parametro3;
    private JPanel decisionView;
    private JTextArea resultado;


    public DecisionMaker() {
        calcularButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resultado.setText(calculate());
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("DecisionMaker");
        frame.setContentPane(new DecisionMaker().decisionView);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private String calculate(){
        return parametro1.getText();
    }

}


